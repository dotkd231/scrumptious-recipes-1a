from django.shortcuts import render
from django.views.generic.list import ListView
from recipes.models import Recipe
from django.views.generic.detail import DetailView
from django.views.generic.edit import UpdateView, CreateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import redirect
from recipes.forms import RatingForm

def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
                rating.save()
            except Recipe.DoesNotExist:
                return redirect("recipes_list")
    return redirect("recipe_detail", pk=recipe_id)

class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"

class RecipeUpdateView(UpdateView):
    model = Recipe
    fields = [
        "name",
        "author",
        "description",
        "image",
    ]
    template_name = "recipes/edit.html"
    success_url = reverse_lazy("recipes_list")

class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"
    success_url = reverse_lazy("recipes_list")
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        return context

class RecipeCreateView(CreateView):
    model = Recipe
    fields = [
        "name",
        "author",
        "description",
        "image",
        ]
    template_name = "recipes/create.html"

class RecipeDeleteView(DeleteView):
    model = Recipe
    success_url = reverse_lazy("recipes_list")    