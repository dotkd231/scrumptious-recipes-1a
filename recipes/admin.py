from django.contrib import admin
from recipes.models import Recipe, Step
from tags.models import Tag
admin.site.register(Recipe)
admin.site.register(Step)
admin.site.register(Tag)